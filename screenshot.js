var glob = require("glob"),
	path = require('path'),
	screenshotDirectory = './-SCREENSHOTS',
	fs = require('fs'),
	webshot = require('webshot');

if(!fs.existsSync(screenshotDirectory)){
	fs.mkdirSync(screenshotDirectory);
}

glob("./WASSERMAN-PRIDE-ROI/*.html", {}, function(er, files){

	files.forEach(function(file){

		var imageName = path.basename(file, '.html') + '.png';
		var projectName = file.split('/')[1];
		var targetDirectory = screenshotDirectory + '/' + projectName;
		
		// make the project folder
		if(!fs.existsSync(targetDirectory)){
			fs.mkdirSync(targetDirectory);
		}

		// Load the file into a string
		fs.readFile(file, 'utf8', function(err, data){

			// Take and save a screenshot
			webshot(data, targetDirectory + '/' + imageName, {siteType:'html', shotSize: 'all', renderDelay: 5}, function(err){
				if(!err){
					console.log('Screenshot saved');
				} else {
					console.log('Failed to make screenshot');
				}
			});

			webshot(data, targetDirectory + '/mobile-' + imageName, {siteType:'html', shotSize: 'all', windowSize: {width: 460, height: 528}, renderDelay: 5}, function(err){
				if(!err){
					console.log('Screenshot saved');
				} else {
					console.log('Failed to make screenshot');
				}
			});

		});


	});

});